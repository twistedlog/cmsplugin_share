from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Tweet(CMSPlugin):
    url = models.URLField()
    sharetext = models.CharField(max_length=140)
    hashtags = models.CharField(
        max_length=80, help_text=_("Comma Seperated List of HashTags"))

    def __unicode__(self):
        return "Tweet {0}, {1}, {2}".format(self.url,
                                            self.sharetext,
                                            self.hashtags)
