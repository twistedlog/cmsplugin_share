from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cmsplugin_share.models import Tweet
from django.utils.translation import ugettext as _


class TweetPlugin(CMSPluginBase):
    model = Tweet
    name = _("Twitter share button")
    render_template = 'cmsplugin_share/tweet.html'

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(TweetPlugin)
